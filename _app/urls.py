from django.urls import include, path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *

app_name = '_app'

urlpatterns = [
    # path('activity',ActivityList.as_view()),
    # path('activity/<int:pk>',ActivityList.as_view()),

    path('cat-package',ParentPackageList.as_view()),
    path('cat-package-detail/<str:slug>',ParentPackageDetail.as_view()),
    path('cat-package-create>',ParentPackageListCreate.as_view()),
    path('cat-package-delete/<int:pk>',ParentPackageDelete.as_view()),

    path('include',IncludeList.as_view()),
    path('include/<int:pk>',IncludeList.as_view()),

    path('exclude',ExcludeList.as_view()),
    path('exclude/<int:pk>',ExcludeList.as_view()),

    path('itinerary',ItineraryList.as_view()),
    path('itinerary/<int:pk>',ItineraryList.as_view()),

    path('price-package',PricePackageList.as_view()),
    path('price-package/<int:pk>',PricePackageList.as_view()),

    path('testimony-list',TestimonyList.as_view()),
    path('testimony-create',TestimonyCreate.as_view()),
    path('testimony/<int:pk>',TestimonyDetail.as_view()),
    path('testimony-update/<int:pk>',TestimonyUpdate.as_view()),
    path('testimony-delete/<int:pk>',TestimonyDelete.as_view()),

    path('gallery-list',GalleryList.as_view()),
    path('gallery-create',GalleryCreate.as_view()),
    path('gallery/<int:pk>',GalleryDetail.as_view()),
    path('gallery-update/<int:pk>',GalleryUpdate.as_view()),
    path('gallery-delete/<int:pk>',GalleryDelete.as_view()),

    path('banner-list',BannerList.as_view()),
    path('banner-create',BannerCreate.as_view()),
    path('banner/<int:pk>',BannerDetail.as_view()),
    path('banner-update/<int:pk>',BannerUpdate.as_view()),
    path('banner-delete/<int:pk>',BannerDelete.as_view()),

    path('booking',BookingList.as_view()),
    path('booking/<int:pk>',BookingList.as_view()),


    path('blog-list',BlogList.as_view()),
    path('blog-create',BlogCreate.as_view()),
    path('blog/<str:slug>',BlogDetail.as_view()),
    path('blog-update/<int:pk>',BlogUpdate.as_view()),
    path('blog-delete/<int:pk>',BlogDelete.as_view()),

    path('package-list',PackageList.as_view()),
    path('package-create',PackageCreate.as_view()),
    path('package/<int:pk>',PackageDetail.as_view()),
    path('package-update/<int:pk>',PackageUpdate.as_view()),
    path('package-delete/<int:pk>',PackageDelete.as_view()),

]

urlpatterns = format_suffix_patterns(urlpatterns)