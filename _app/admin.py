from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Itinerary)
admin.site.register(Package)
admin.site.register(ParentPackage)
admin.site.register(Include)
admin.site.register(Exclude)
admin.site.register(Destination)
admin.site.register(PricePackage)
admin.site.register(Testimony)
admin.site.register(Gallery)
admin.site.register(Banner)
admin.site.register(Booking)
admin.site.register(Blog)



