from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import *

# Activity

# class ActivitySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Activity
#         fields = "__all__"

    
class IncludeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Include
        fields = "__all__"

class ItinerarySerializer(serializers.ModelSerializer):
    # activity = ActivitySerializer(many=True, read_only=True)
    class Meta:
        model = Itinerary
        fields = "__all__"


class PackageSerializer(serializers.ModelSerializer):
    includes = IncludeSerializer(many=True, read_only=True)
    excludes = IncludeSerializer(many=True, read_only=True)
    destinations = IncludeSerializer(many=True, read_only=True)
    itinerary = ItinerarySerializer(many=True, read_only=True)
    class Meta:
        model = Package
        fields = "__all__"

class ParentPackageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParentPackage
        fields = "__all__"

class ExcludeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Exclude
        fields = "__all__"


class PricePackageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PricePackage
        fields = "__all__"

class TestimonySerializer(serializers.ModelSerializer):
    class Meta:
        model = Testimony
        fields = "__all__"


class GallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Gallery
        fields = "__all__"


class BannerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Banner
        fields = "__all__"

class BookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Booking
        fields = "__all__"

class BlogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Blog
        fields = "__all__"



