from rest_framework import viewsets
from rest_framework import generics
from rest_framework.parsers import MultiPartParser


from .models import *
from .serializers import *

from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

# class ActivityList(APIView):

#     def get_object(self, pk):
#         try:
#             return Activity.objects.get(pk=pk)
#         except Activity.DoesNotExist:
#             raise Http404

    
#     # getallData and detil data with id
#     def get(self, request, pk=None):
#         if pk is None:
#              activitys = Activity.objects.all()
#              serializer = ActivitySerializer(activitys, many=True)
#              return Response(serializer.data)
#         else:
#             try:
#                 detail_data = Activity.objects.get(pk=pk)
#                 serializer = ActivitySerializer(detail_data, many=False)
#                 return Response(serializer.data)
        
#             except Activity.DoesNotExist:
#                 return Response(
#                     {'error':'Data not found'},
#                     status=status.HTTP_204_NO_CONTENT
#                 )

#     # postData
#     def post(self, request):
#         serializer = ActivitySerializer(data=request.data)
        
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#     def delete(self, request, pk, format=None):
#         activity = self.get_object(pk)
#         activity.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)
    
#     def put(self, request, pk, format=None):
#         datas = self.get_object(pk)
#         serializer = ActivitySerializer(datas, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class IncludeList(APIView):

    def get_object(self, pk):
        try:
            return Include.objects.get(pk=pk)
        except Package.DoesNotExist:
            raise Http404

    def get(self, request, pk=None):
        if pk is None:
            includes = Include.objects.all()
            serializer = IncludeSerializer(includes, many=True)
            return Response(serializer.data)
        else:
            try:
                detail_data = Include.objects.get(pk=pk)
                serializer = IncludeSerializer(detail_data, many=False)
                return Response(serializer.data)
        
            except Package.DoesNotExist:
                return Response(
                    {'error':'Data not found'},
                    status=status.HTTP_204_NO_CONTENT
                )
    
    def post(self, request, format=None):
        serializer = IncludeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request,  pk, format=None):
        include = self.get_object(pk)
        include.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    def put(self, request, pk, format=None):
        dta = self.get_object(pk=pk)
        serializer= IncludeSerializer(dta, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ExcludeList(APIView):

    def get_object(self, pk):
        try:
            return Exclude.objects.get(pk=pk)
        except Package.DoesNotExist:
            raise Http404

    def get(self, request, pk=None):
        if pk is None:
            Excludes = Exclude.objects.all()
            serializer = ExcludeSerializer(Excludes, many=True)
            return Response(serializer.data)
        else:
            try:
                detail_data = Exclude.objects.get(pk=pk)
                serializer = ExcludeSerializer(detail_data, many=False)
                return Response(serializer.data)
        
            except Package.DoesNotExist:
                return Response(
                    {'error':'Data not found'},
                    status=status.HTTP_204_NO_CONTENT
                )
    
    def post(self, request, format=None):
        serializer = ExcludeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request,  pk, format=None):
        Exclude = self.get_object(pk)
        Exclude.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    def put(self, request, pk, format=None):
        dta = self.get_object(pk=pk)
        serializer= ExcludeSerializer(dta, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ItineraryList(APIView):

    def get_object(self, pk):
        try:
            return Itinerary.objects.get(pk=pk)
        except Package.DoesNotExist:
            raise Http404

    def get(self, request, pk=None):
        if pk is None:
            Itinerarys = Itinerary.objects.all()
            serializer = ItinerarySerializer(Itinerarys, many=True)
            return Response(serializer.data)
        else:
            try:
                detail_data = Itinerary.objects.get(pk=pk)
                serializer = ItinerarySerializer(detail_data, many=False)
                return Response(serializer.data)
        
            except Package.DoesNotExist:
                return Response(
                    {'error':'Data not found'},
                    status=status.HTTP_204_NO_CONTENT
                )
    
    def post(self, request, format=None):
        serializer = ItinerarySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request,  pk, format=None):
        Itinerary = self.get_object(pk)
        Itinerary.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    def put(self, request, pk, format=None):
        dta = self.get_object(pk=pk)
        serializer= ItinerarySerializer(dta, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PricePackageList(APIView):

    def get_object(self, pk):
        try:
            return PricePackage.objects.get(pk=pk)
        except Package.DoesNotExist:
            raise Http404

    def get(self, request, pk=None):
        if pk is None:
            PricePackages = PricePackage.objects.all()
            serializer = PricePackageSerializer(PricePackages, many=True)
            return Response(serializer.data)
        else:
            try:
                detail_data = PricePackage.objects.get(pk=pk)
                serializer = PricePackageSerializer(detail_data, many=False)
                return Response(serializer.data)
        
            except Package.DoesNotExist:
                return Response(
                    {'error':'Data not found'},
                    status=status.HTTP_204_NO_CONTENT
                )
    
    def post(self, request, format=None):
        serializer = PricePackageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request,  pk, format=None):
        PricePackage = self.get_object(pk)
        PricePackage.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    def put(self, request, pk, format=None):
        dta = self.get_object(pk=pk)
        serializer= PricePackageSerializer(dta, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# testimoy
class TestimonyList(generics.ListAPIView):
    queryset = Testimony.objects.all()  
    serializer_class = TestimonySerializer

class TestimonyCreate(generics.CreateAPIView):
    queryset = Testimony.objects.all()  
    serializer_class = TestimonySerializer

class TestimonyDetail(generics.RetrieveAPIView):
    queryset = Testimony.objects.all()  
    serializer_class = TestimonySerializer
    lookup_field = 'pk'

class TestimonyUpdate(generics.UpdateAPIView):
    queryset = Testimony.objects.all()  
    serializer_class = TestimonySerializer
    lookup_field = 'pk'
  
class TestimonyDelete(generics.DestroyAPIView):
    queryset = Testimony.objects.all()  
    serializer_class = TestimonySerializer
    lookup_field = 'pk'

# endTestimony
  

# gallery

class GalleryList(generics.ListAPIView):
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer

class GalleryCreate(generics.CreateAPIView):
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer

class GalleryDetail(generics.RetrieveAPIView):
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer
    lookup_field = 'pk'

class GalleryUpdate(generics.UpdateAPIView):
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer
    lookup_field = 'pk'

class GalleryDelete(generics.DestroyAPIView):
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer
    lookup_field = 'pk'

# endGallery

# Banner
class BannerList(generics.ListAPIView):
    queryset = Banner.objects.all()
    serializer_class = BannerSerializer

class BannerCreate(generics.CreateAPIView):
    queryset = Banner.objects.all()
    serializer_class = BannerSerializer
    
class BannerDetail(generics.RetrieveAPIView):
    queryset = Banner.objects.all()
    serializer_class = BannerSerializer
    lookup_field = 'pk'

class BannerUpdate(generics.UpdateAPIView):
    queryset = Banner.objects.all()
    serializer_class = BannerSerializer
    lookup_field = 'pk'

class BannerDelete(generics.DestroyAPIView):
    queryset = Banner.objects.all()
    serializer_class = BannerSerializer
    lookup_field = 'pk'
# endBanner

# Booking
class BookingList(APIView):

    def get_object(self, pk):
        try:
            return Booking.objects.get(pk=pk)
        except Package.DoesNotExist:
            raise Http404

    def get(self, request, pk=None):
        if pk is None:
            Bookings = Booking.objects.all()
            serializer = BookingSerializer(Bookings, many=True)
            return Response(serializer.data)
        else:
            try:
                detail_data = Booking.objects.get(pk=pk)
                serializer = BookingSerializer(detail_data, many=False)
                return Response(serializer.data)
        
            except Package.DoesNotExist:
                return Response(
                    {'error':'Data not found'},
                    status=status.HTTP_204_NO_CONTENT
                )
    
    def post(self, request, format=None):
        serializer = BookingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request,  pk, format=None):
        Booking = self.get_object(pk)
        Booking.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    def put(self, request, pk, format=None):
        dta = self.get_object(pk=pk)
        serializer= BookingSerializer(dta, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
# endBookig

# blog
class BlogList(generics.ListAPIView):
    queryset=Blog.objects.all()
    serializer_class=BlogSerializer

class BlogCreate(generics.CreateAPIView):
    queryset=Blog.objects.all()
    serializer_class=BlogSerializer

class BlogDetail(generics.RetrieveAPIView):
    queryset=Blog.objects.all()
    serializer_class=BlogSerializer
    lookup_field='slug'

class BlogUpdate(generics.UpdateAPIView):
    queryset=Blog.objects.all()
    serializer_class=BlogSerializer
    lookup_field='pk'

class BlogDelete(generics.DestroyAPIView):
    queryset=Blog.objects.all()
    serializer_class=BlogSerializer
    lookup_field='pk'
# edBlog

# catPaket
class ParentPackageList(generics.ListAPIView):
    queryset=ParentPackage.objects.all()
    serializer_class=ParentPackageSerializer

class ParentPackageDetail(generics.RetrieveAPIView):
    queryset = ParentPackage.objects.all()
    serializer_class = ParentPackageSerializer

    def retrieve(self, request, *args, **kwargs):
        slug = self.kwargs.get('slug')
        instance = get_object_or_404(ParentPackage, slug=slug)
        children = Package.objects.filter(cat_paket=instance)

        serializer = self.get_serializer(instance)
        data = serializer.data
        data['package'] = PackageSerializer(children, many=True).data

        return Response(data)


class ParentPackageListCreate(generics.CreateAPIView):
    queryset=ParentPackage.objects.all()
    serializer_class=ParentPackageSerializer

class ParentPackageDelete(generics.DestroyAPIView):
    queryset=ParentPackage.objects.all()
    serializer_class=ParentPackageSerializer
    lookup_field='pk'




# package
class PackageList(generics.ListAPIView):
    queryset=Package.objects.all()
    serializer_class=PackageSerializer    

class PackageCreate(generics.CreateAPIView):
    queryset=Package.objects.all()
    serializer_class=PackageSerializer

class PackageDetail(generics.RetrieveAPIView):
    queryset=Package.objects.all()
    serializer_class=PackageSerializer
    lookup_field='pk'

class PackageUpdate(generics.UpdateAPIView):
    queryset=Package.objects.all()
    serializer_class=PackageSerializer
    lookup_field='pk'

class PackageDelete(generics.DestroyAPIView):
    queryset=Package.objects.all()
    serializer_class=PackageSerializer
    lookup_field='pk'
# edpackage