from django.db import models
from django.utils.text import slugify
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.
import sys
from PIL import Image

from distutils.command.upload import upload
from email.policy import default
from statistics import mode
from django.urls import reverse

from django.utils.text import slugify


CATEGORY_CHOICES = (
    ('Paket Honeymoon','Paket Honeymoon'),
    ('Paket Tour','Paket Lombok'),
    ('Paket Harian','Paket Harian'),

)


class Destination(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to='imgDestinations')
    description = models.TextField()

    def __str__(self):
        return self.name

class ParentPackage(models.Model):
    name = models.CharField(max_length=100)
    category = models.CharField(max_length=200, choices=CATEGORY_CHOICES, default='')
    price = models.CharField(max_length=100)
    image = models.ImageField(upload_to='parentPackage')
    slug = models.SlugField(blank=True, null=True, editable=False)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)
    
    def get_children(self):
        return Package.objects.filter(cat_paket=self)

    def __str__(self):
        return self.name

class Exclude(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Include(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name



class Itinerary(models.Model):
    name = models.CharField(max_length=100)
    day = models.CharField(max_length=50)
    activity = RichTextUploadingField()

    def __str__(self):
        return self.name

class Package(models.Model):
    name = models.CharField(max_length=100)
    cat_paket = models.ForeignKey(ParentPackage, related_name='parent_package', default="", on_delete=models.CASCADE)
    durations = models.CharField(max_length=100)
    destinations = models.ManyToManyField(Destination)
    desc = models.TextField()
    price = models.CharField(max_length=100)
    status = models.CharField(max_length=1)
    image = models.ImageField(upload_to='imgPackage')
    image_price = models.ImageField(upload_to='imgPrice', default='')
    includes = models.ManyToManyField(Include)
    excludes = models.ManyToManyField(Exclude)
    itinerary = models.ManyToManyField(Itinerary)

    def __str__(self):
        return self.name

# price

class PricePackage(models.Model):
    name = models.CharField(max_length=200)
    harga_paket = models.ForeignKey(Package, related_name='package_price', default="", on_delete=models.CASCADE)
    jumlah_peserta = models.CharField(max_length=200, null=True, blank=True)
    hotel_bintang2= models.CharField(max_length=200, null=True, blank=True)
    hotel_bintang3= models.CharField(max_length=200, null=True, blank=True)
    hotel_bintang4= models.CharField(max_length=200, null=True, blank=True)
    hotel_bintang5= models.CharField(max_length=200, null=True, blank=True)
    tanpa_hotel= models.CharField(max_length=200)

    def __str__(self):
        return str(self.name)


class Testimony(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to="userTestimony", default='', null=True, blank=True)
    komentar = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.name


class Gallery(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='gallery', default='')
    desc = models.TextField()

    def __str__(self):
        return self.name


class Banner(models.Model):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to="banner", default='')

    def __str__(self):
        return self.title


class Booking(models.Model):
    name          = models.CharField(max_length=200)
    address        = models.CharField(max_length=200)
    phone_number      = models.CharField(max_length=200)
    jumlah_peserta  = models.CharField(max_length=100)
    tanggal_jemput= models.DateField()
    package         = models.ForeignKey(Package, on_delete=models.CASCADE)
    status = models.CharField(max_length=5) 
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Blog(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='blog_image',default="")
    content = RichTextUploadingField(default="")
    tanggal = models.DateField(auto_now=True)
    penulis = models.CharField(max_length=100)
    slug = models.SlugField(blank=True, null=True, editable=False)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)

    def __str__(self) -> str:
        return self.title

